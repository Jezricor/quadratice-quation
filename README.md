// The code of a small programmer.
// This is my first program, which I will post here.
// You can not use it, but for me it's important that I know that I did not just work.
// This program solves the quadratic equation. If you have an objection, then instruct me on the correct way of programming.
// So, let's get started.

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	cout << "This program is designed to solve the quadratic equation. You will need to enter the numbers to be assigned a, b, c (ax^2+bx+c)";

	double a, b, c; // coefficients of equation
	double d; // discriminant
	double x1, x2; // the roots of equation

	cout << "\nEnter the a: ";
	cin >> a;

	if (a == 0)
	{
		a = 1;
	}

	cout << "Ok, now type b: ";
	cin >> b;
	cout << "And now c: ";
	cin >> c;

	d = (b * b) - (4 * a * c); // discriminant formula
	cout << "D = b^2 - 4*a*c" << endl;
	cout << "D = " << d << endl;

	if (d < 0)
	{
		cout << "Roots are not present.\n";
	}

	else if (d == 0)
	{
		x1 = -b / (2 * a);
		cout << "1 root: " << x1 << endl;
	}

	else
	{
		x1 = (-b - sqrt(d)) / (2 * a);
		x2 = (-b + sqrt(d)) / (2 * a);
		cout << "x1 = " << x1 << endl;
		cout << "x2 = " << x2 << endl;
	}

	system("pause");

	return 0;
}